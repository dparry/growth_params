# Growth Parameter Calculations

Simple python utilities for calculating Z-scores for human growth data (height,
weight, OFC).

## Installation/Usage

Requires python >=3.5

    pip3 install git+https://git.ecdf.ed.ac.uk/dparry/growth_params.git --user

You may also choose to clone this repository and see the examples.ipynb
notebook for example use cases.

    git clone https://git.ecdf.ed.ac.uk/dparry/growth_params.git
    cd growth_params
    jupyter-notebook
    
